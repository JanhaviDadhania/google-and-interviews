
// Given an Array A, find the minimum amplitude you can get after changing up to 3 elements. Amplitude is the range of the array (basically difference between largest and smallest element).
// https://leetcode.com/discuss/interview-question/553399/

#include<bits/stdc++.h>
using namespace std;
int main() {
    int n,input;
    vector<int> v;
    cin>>n;
    for(int i=0;i<n;i++) {
        cin>>input;
        v.push_back(input);
    }

    int one_min = INT_MAX, two_min = INT_MAX, three_min = INT_MAX, four_min = INT_MAX;
    int one_max = INT_MIN, two_max = INT_MIN, three_max = INT_MIN, four_max = INT_MIN;

    for (int i=0;i<n;i++) {
        if (v[i] < one_min) {
            one_min = v[i];
        }
    }
    for (int i=0;i<n;i++) {
        if (v[i] != one_min && v[i]<two_min) {
            two_min = v[i];
        }
    }
    for (int i=0;i<n;i++) {
        if (v[i] != one_min && v[i] != two_min && v[i]<three_min) {
            three_min = v[i];
        }
    }
    for (int i=0;i<n;i++) {
        if (v[i] != one_min && v[i] != two_min && v[i] != three_min && four_min > v[i]) {
            four_min = v[i];
        }
    }

    for (int i=0;i<n;i++) {
        if (v[i] > one_max) {
            one_max = v[i];
        }
    }
    for (int i=0;i<n;i++) {
        if (v[i] != one_max && v[i] > two_max) {
            two_max = v[i];
        }
    }
    for (int i=0;i<n;i++) {
        if (v[i] != one_max && v[i] != two_max && v[i] > three_max) {
            three_max = v[i];
        }
    }
    for (int i=0;i<n;i++) {
        if (v[i] != one_max && v[i] != two_max && v[i] != three_max && v[i] > four_max) {
            four_max = v[i];
        }
    }

    if ((two_min - one_min) > (one_max - two_max)) {
        cout<<one_min;
        if ((three_min - two_min) > (one_max - two_max)) {
            cout<<two_min;
            if ((four_min - three_min) > (one_max - two_max)) {
                cout<<three_min;
            }
            else {
                cout<<one_max;
            }
        }
        else {
            cout<<one_max;

        }
    }
    else {
        cout<<one_max;
        if ((two_min - one_min) < (two_max - three_max)) {
            cout<<two_max;
            if ((two_min - one_min) < (three_max - four_max)) {
                cout<<three_max;
            }
            else {
                cout<<one_min;
            }
        }
        else{
            cout<<one_min;
            if ((three_min - two_min) < (two_max - three_max)) {
                cout<<two_max;
            }
            else {
                cout<<two_min;
            }
        }
    }

    return 0;
}
