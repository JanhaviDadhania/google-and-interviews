// Given a string S, we can split S into 2 strings: S1 and S2. Return the number of ways S can be split such that the number of unique characters between S1 and S2 are the same.
// https://leetcode.com/discuss/interview-question/553399/
#include<bits/stdc++.h>
using namespace std;
int main() {
    string s;
    cin>>s;
    int length = s.length();

    vector<int> seen(128, 0);
    vector<int> right(length, 0);
    for(int i=length-1;i>=0;i--) {
        if (i != length-1) {
            right[i] = right[i+1];
        }
        if (!seen[s[i]]) {
            seen[s[i]] = 1;
            ++right[i];
        }
    }

    for(int i=0;i<right.size();i++) {
        cout<<right[i]<<" ";
    }cout<<endl;

   int count = 0;
    vector<int> seen_two(128, 0);
    int left = 0;
    for(int i=0;i<length;i++) {
        if (!seen_two[s[i]]) {
            ++left;
            seen_two[s[i]] = 1;
        }
        if (left == right[i+1]) {
            cout<<i<<" "<<left<<right[i+1];
            count++;
        }
    }

    cout<<endl<<count<<endl;
    return 0;
}
