one = input().split(",")
two = input().split(",")
frequency = [0] * 11

for w in one:
    count = w.count(min(w))
    frequency[count] = frequency[count] + 1

for w in two:
    count = w.count(min(w))
    print(sum(frequency[:count]))
